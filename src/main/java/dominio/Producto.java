package dominio;

public class Producto {
	
	private int id;
	private String nombre;
	private boolean aplicaIva;
	private String tipo;
	private String presentacion;
	private String color;
	
	public Producto() {
	}

	public Producto(int id, String nombre, boolean aplicaIva, String tipo, String presentacion, String color) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.aplicaIva = aplicaIva;
		this.tipo = tipo;
		this.presentacion = presentacion;
		this.color = color;
	}

	public Producto(int id, String nombre, boolean aplicaIva, String tipo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.aplicaIva = aplicaIva;
		this.tipo = tipo;
		this.presentacion = "Unidad";
		this.color = "NA";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isAplicaIva() {
		return aplicaIva;
	}

	public void setAplicaIva(boolean aplicaIva) {
		this.aplicaIva = aplicaIva;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	
}
