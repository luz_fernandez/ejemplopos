package ui;

import java.util.ArrayList;

import dominio.Producto;

public class Runner {
	
	public static void main(String[] args) {
		
		String[] tiposProductos = {"Canasta Familiar", "Licores", "Aseo"};
			
		for (int i = 0; i < tiposProductos.length; i++) {
		//	System.out.println( i + " "+ tiposProductos[i] );
		}
		
		int[] enteros = new int[10];
		
		for (int i = 0; i < enteros.length; i++) {
			//enteros[i] = i;
			//System.out.print(enteros[i] + ", ");
		}
		
		int contador = 10;
		while (contador <= 5 ) {
			System.out.println("Contador es menor que 5, "
					+ "actualmente es: " + contador);
			contador += 3;
		}
		
		do {
			//System.out.println("Contador es menor que 5, "
				//	+ "actualmente es: " + contador + " desde do");
		}while(contador <= 5);
		
		ArrayList<Producto> listaCompra = new ArrayList<Producto>();
		
		listaCompra.add(new Producto(1, "Gaseosa", false, tiposProductos[0]));
		listaCompra.add(new Producto(2, "Pan", false, tiposProductos[0]));
		
		System.out.println((listaCompra.get(1)).getNombre());
		
	}

}
