package aplicacion.servicios;

import java.util.HashMap;

import dominio.Producto;

public class Inventario {

	private HashMap<Producto, Integer> stock = new HashMap<Producto, Integer>();

	public void agregarEntrada(Producto producto, int cantidad) {
		stock.put(producto, stock.get(producto) + cantidad);
	}

	public void agregarSalida(Producto producto, int cantidad) {
		stock.put(producto, stock.get(producto) - cantidad);
	}

}
